/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/Worklist",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/Object",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/NotFound",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/Browser",
	"com/infocus/fi/tax/ztaxapp/test/integration/pages/App"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.infocus.fi.tax.ztaxapp.view."
	});

	sap.ui.require([
		"com/infocus/fi/tax/ztaxapp/test/integration/WorklistJourney",
		"com/infocus/fi/tax/ztaxapp/test/integration/ObjectJourney",
		"com/infocus/fi/tax/ztaxapp/test/integration/NavigationJourney",
		"com/infocus/fi/tax/ztaxapp/test/integration/NotFoundJourney",
		"com/infocus/fi/tax/ztaxapp/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});