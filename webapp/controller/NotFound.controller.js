sap.ui.define([
		"com/infocus/fi/tax/ztaxapp/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("home");
			}

		});

	}
);