sap.ui.define(
  [
    "com/infocus/fi/tax/ztaxapp/controller/BaseController",
    "sap/m/MessageToast",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/FilterOperator",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/ushell/library",
    "sap/m/library",
    "sap/m/Label",
    "sap/m/Text",
    "sap/m/TextArea",
    "sap/ui/core/Core"
  ],
  function (BaseController,MessageToast,Filter,Fragment,FilterOperator,Button,Dialog,SapUshellLib,mobileLibrary, Label, Text,
    TextArea, Core) {
    "use strict";
    var ButtonType = mobileLibrary.ButtonType;

	  // shortcut for sap.m.DialogType
	  var DialogType = mobileLibrary.DialogType;

    return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.Home", {
      pressDialog: null,
      onInit: function () {
        //	var oRouter = this.getRouter();
        //	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
        // Hint: we don't want to do it this way
        /*
			 oRouter.attachRouteMatched(function (oEvent){
				 var sRouteName, oArgs, oView;
				 sRouteName = oEvent.getParameter("name");
				 if (sRouteName === "employee"){
				 	this._onRouteMatched(oEvent);
				 }
			 }, this);
			 */
        this.getUserIdFromLoggedInUser();
      },

      getUserIdFromLoggedInUser: function () {
        var that = this;
        var userId = "1000";
        that.onGlobalUserIdSet(userId);
        that._onReadYearSet();
        /*sap.ushell.Container.getServiceAsync("UserInfo").then(function(UserInfo) {
				//var userId = UserInfo.getId();
				var userId = "1000";
				that.onGlobalUserIdSet(userId);
				that._onReadYearSet();
			});*/
      },
      _callInitialTab: function () {
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "get80CData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "get80Data", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getHRAData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getPreviousEmployeeData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getHousePropertyData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getOtherSourcData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getTaxRegimeData", {});
        this.getOwnerComponent()
          .getEventBus()
          .publish("Default", "getIncomeTaxReport", {});
      },
      onChange: function (oEvent) {
        var selKey = oEvent.getParameter("selectedItem").getKey();
        var selText = oEvent.getParameter("selectedItem").getText();
        var oJsonGlobalModel = this.getOwnerComponent().getModel("globalData");
        var oJsonGlobalData = oJsonGlobalModel.getData();
        var oSelectedYear = oJsonGlobalData.selectedYear;
        oSelectedYear.yearKey = selKey;
        oSelectedYear.yearText = selText;
        oJsonGlobalModel.setData(oJsonGlobalData);
      },
      onFiancialYearDialogPressOld: function () {
        var that = this;
        if (!this._oDialog) {
          this._oDialog = sap.ui.xmlfragment(
            "com.infocus.fi.tax.ztaxapp.view.fragment.Dialog",
            this
          );
          this._oDialog.setModel(that.getOwnerComponent().getModel("yearSet"));
          this.getView().addDependent(this._oDialog);
        }
        this._oDialog.open();
      },
      onFiancialYearDialogPress: function (oEvent) {
        var oButton = oEvent.getSource(),
          oView = this.getView();
        if (!this._pDialog) {
          var that = this;
          this._pDialog = Fragment.load({
            id: oView.getId(),
            name: "com.infocus.fi.tax.ztaxapp.view.fragment.Dialog",
            controller: this,
          }).then(function (oDialog) {
            oDialog.setModel(that.getOwnerComponent().getModel("yearSet"));
            return oDialog;
          });
        }

        this._pDialog.then(
          function (oDialog) {
            oDialog.setBeginButton(
              new Button({
                text: "Close",
                press: function (oEvent) {
                  this.onDialogClose(oEvent);
                  this.pressDialog.close();
                }.bind(this),
              })
            );
            //this._configDialog(oButton, oDialog);
            oDialog.open();
          }.bind(this)
        );
      },
      _configDialog: function (oButton, oDialog) {
        var sCustomConfirmButtonText = !!oButton.data("confirmButtonText");
        oDialog.setConfirmButtonText(sCustomConfirmButtonText);
        //add Clear button if needed
        var bShowClearButton = !!oButton.data("showClearButton");
        oDialog.setShowClearButton(bShowClearButton);
        // Set style classes
        var sResponsiveStyleClasses =
          "sapUiResponsivePadding--header sapUiResponsivePadding--subHeader sapUiResponsivePadding--content sapUiResponsivePadding--footer";
        var bResponsivePadding = !!oButton.data("responsivePadding");
        oDialog.toggleStyleClass(sResponsiveStyleClasses, bResponsivePadding);
      },
      onSearch: function (oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new Filter("Fiscal", FilterOperator.Contains, sValue);
        var oBinding = oEvent.getParameter("itemsBinding");
        oBinding.filter([oFilter]);
      },

      onDialogClose: function (oEvent) {
        var aContexts = oEvent.getParameter("selectedContexts");
        if (aContexts && aContexts.length) {
          var year = aContexts
            .map(function (oContext) {
              return oContext.getObject().Fiscal;
            })
            .join(", ");
          MessageToast.show("You have chosen " + year);
          this.onDefaultYearSelect(year);
          this._callInitialTab();
          this.getEmployeePersonalData();
        } else {
          MessageToast.show("No new item was selected.");
        }
        oEvent.getSource().getBinding("items").filter([]);
      },
      onDefaultYearSelect: function (oData) {
        var oJsonGlobalModel = this.getOwnerComponent().getModel("globalData");
        var oJsonGlobalData = oJsonGlobalModel.getData();
        oJsonGlobalData.selectedYear =
          oJsonGlobalData.selectedYear === undefined
            ? ""
            : oJsonGlobalData.selectedYear;
        oJsonGlobalData.selectedYear = oData;
        oJsonGlobalModel.setData(oJsonGlobalData);
      },
      onGlobalUserIdSet: function (oUserId) {
        var oJsonGlobalModel = this.getOwnerComponent().getModel("globalData");
        var oJsonGlobalData = oJsonGlobalModel.getData();
        oJsonGlobalData.userId =
          oJsonGlobalData.userId === undefined ? "" : oJsonGlobalData.userId;
        oJsonGlobalData.userId = oUserId;
        oJsonGlobalModel.setData(oJsonGlobalData);
      },
      onCurrentFiscalYear: function (oFiscalYear) {
        var oJsonGlobalModel = this.getOwnerComponent().getModel("globalData");
        var oJsonGlobalData = oJsonGlobalModel.getData();

        oJsonGlobalData.currentFiscalYear = oFiscalYear;
        oJsonGlobalModel.setData(oJsonGlobalData);
      },
      _onReadYearSet: function () {
        var that = this;
        var oModel = this.getOwnerComponent().getModel();
        var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, "40000066");
        // The first parameter of filter is path, operator(eq=equals), value.
        //var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
        //var oPernerFilter = new Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
        var oUrl = "/YEARSet";

        oModel.read(oUrl, {
          filters: [oPernerFilter],
          success: function (response) {
            var data = response.results;
            var oJsonYearSetModel = that
              .getOwnerComponent()
              .getModel("yearSet");
            oJsonYearSetModel.setData(data);
            that.onDefaultYearSelect(data[0].Fiscal);
            that.getEmployeePersonalData();
				that._callInitialTab();
          },
          error: function (error) {},
        });
        oModel.attachRequestCompleted(function () {}.bind(this));
      },
      _onRouteMatched: function (oEvent) {
        var oArgs, oView;
        oArgs = oEvent.getParameter("arguments");
        oView = this.getView();

        oView.bindElement({
          path: "/Employees(" + oArgs.employeeId + ")",
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function (oEvent) {
              oView.setBusy(true);
            },
            dataReceived: function (oEvent) {
              oView.setBusy(false);
            },
          },
        });
      },

      _onBindingChange: function (oEvent) {
        // No data for the binding
        if (!this.getView().getBindingContext()) {
          this.getRouter().getTargets().display("notFound");
        }
      },

      getEmployeePersonalData: function () {
        var that = this;

        var oModel = this.getOwnerComponent().getModel();
        var oJsonGlobalData = this.getOwnerComponent()
          .getModel("globalData")
          .getData();
        //var oPernerFilter = new Filter("personnelNo", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
        //var oPernerFilter = new Filter("personnelNo", sap.ui.model.FilterOperator.EQ, "1000");
        //var oFiscalYearFilter = new Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, "2018-2019");
        var oFiscalYearFilter = new Filter(
          "fiscalYear",
          sap.ui.model.FilterOperator.EQ,
          oJsonGlobalData.selectedYear
        );
        var oUrl = "/TaxHeaderSet";

        oModel.read(oUrl, {
          filters: [oFiscalYearFilter],
          success: function (response) {
            var data = response.results;
            var Email = data[0].email;
            data[0].email = Email.toLowerCase();

            // income Tax current fiscal year property
            /*var currentLenderFiscal = data[0].lender;*/

            that.onCurrentFiscalYear(data[0].lender);

            /*oJsonGlobalData.currentLenderFiscal = currentLenderFiscal;
					
					oJsonGlobalModel.setData(oJsonGlobalData);
					
					console.log(oJsonGlobalData);*/

            var oJEmployeeModel = that
              .getOwnerComponent()
              .getModel("employeeData");
            oJEmployeeModel.setData(data);
            /*console.log(data);*/
          },
          error: function (error) {
            //console.log(error);
            MessageToast.show("Error in loading the Financial Years" + error);
          },
        });
      },

      onDisplayNotFound: function () {
        // display the "notFound" target without changing the hash
        this.getRouter().getTargets().display("notFound", {
          fromTarget: "home",
        });
      },
      onSave: function() {
        var that = this;
        if (!this.oSubmitDialog) {
          this.oSubmitDialog = new Dialog({
            type: DialogType.Message,
            title: "Confirm",
            content: [
              new Label({
                text: "Enter any Text to Submit the Tax Details  for Approval",
                labelFor: "submissionNote"
              }),
              new TextArea("submissionNote", {
                width: "100%",
                placeholder: "Add note (required)",
                liveChange: function(oEvent) {
                  var sText = oEvent.getParameter("value");
                  this.oSubmitDialog.getBeginButton().setEnabled(sText.length > 0);
                }.bind(this)
              })
            ],
            beginButton: new Button({
              type: ButtonType.Emphasized,
              text: "Submit",
              enabled: false,
              press: function() {
                var sText = Core.byId("submissionNote").getValue();
                that.sendForApproval();
                this.oSubmitDialog.close();
              }.bind(this)
            }),
            endButton: new Button({
              text: "Cancel",
              press: function() {
                this.oSubmitDialog.close();
              }.bind(this)
            })
          });
        }
  
        this.oSubmitDialog.open();
      },
      sendForApproval: function() {
        var flagX = "X";
        var sText = "";
  
        this.getModel().metadataLoaded().then(function() {
          var that = this;
          var oUrl = "/TaxHeaderSet";
          var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
          var oJEmployeeModelData = this.getOwnerComponent().getModel('employeeData').getData();
          var postData = {
            "personnelNo": oJEmployeeModelData[0].personnelNo,
            "fiscalYear": oJsonGlobalData.selectedYear,
            "app_ind": flagX,
            "remarks": sText
          };
  
          this.getModel().create(oUrl, postData, {
            success: function(response) {
              that.getEmployeePersonalData();
              MessageToast.show("The response is send for Approval");
            },
            error: function(error) {
              //MessageBox.information("The response connot be saved");
              MessageToast.show("The response connot be saved");
            }
          });
  
        }.bind(this));
      },
      
      //Method for Year Confirmation
      onDialogClose: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				var year = aContexts.map(function(oContext) {
					return oContext.getObject().Fiscal;
				}).join(", ");
				MessageToast.show("You have chosen " + year);
				this.onDefaultYearSelect(year);
				this._callInitialTab();
				this.getEmployeePersonalData();
			} else {
				MessageToast.show("No new item was selected.");
			}
			oEvent.getSource().getBinding("items").filter([]);
		}
      
    });
  }
);