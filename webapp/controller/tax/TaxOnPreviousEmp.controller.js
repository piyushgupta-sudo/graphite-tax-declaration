sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/m/UploadCollectionParameter",
	"com/infocus/fi/tax/ztaxapp/model/formatter",
], function(BaseController, Fragment, MessageBox, MessageToast, Filter, UploadCollectionParameter, formatter) {
	"use strict";
	

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.TaxOnPreviousEmp", {
         formatter : formatter,
		onInit: function() {
			//var oRouter = this.getRouter();

			//	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
			//this._onReadPreviousEmployementDataSet();
			this._initialDisplay();
			this.getOwnerComponent().getEventBus().subscribe("Default", "getPreviousEmployeeData", () => {
				this._onReadPreviousEmployementDataSet();
			});
		},
		onCancelPress: function(oEvent) {
			this._oValueOnFileUpload.close();
		},
		onCancelPressView: function(oEvent) {
			this._oValueOnFileView.close();
this.byId('save').setVisible(false);
					this.byId('cancel').setVisible(false);
		},
		openFileUpload: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oPreviousEmpData = this.getView().getModel("PreviousEmployement").getData();
			oPreviousEmpData.buttonValue = buttonValue;
			this.getView().getModel("PreviousEmployement").setData(oPreviousEmpData);

			if (!this._oValueOnFileUpload) {
				this._oValueOnFileUpload = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileUploadPreviousEmp", this);
				this._oValueOnFileUpload.setModel(that.getOwnerComponent().getModel("FileUploadPreviousEmp"));
				this.getView().addDependent(this._oValueOnFileUpload);
			}
			this._oValueOnFileUpload.open();
			var btnPreviousEmpViewFile = that.getView().byId("btnPreviousEmpViewFile");
			btnPreviousEmpViewFile.setEnabled(true);
		},
		onBeforeUploadStarts: function(oEvent) {
			var oPreviousEmpData = this.getView().getModel("PreviousEmployement").getData();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionPreviousEmp");
			var cFiles = oUploadCollection.getItems().length;
			var oComponentName = oPreviousEmpData.buttonValue;

			var oValue = [];
			oValue.push(oComponentName);
			oValue.push(oEvent.getParameter("fileName"));

			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: oValue
			});

			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		},
		viewUploadedFile: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oPreviousEmpData = this.getView().getModel("PreviousEmployement").getData();
			oPreviousEmpData.buttonValue = buttonValue;
			this.getView().getModel("PreviousEmployement").setData(oPreviousEmpData);
			

			// fragment open 
			if (!this._oValueOnFileView) {
				this._oValueOnFileView = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.view.FileUploadPreviousEmpView", this);
				this._oValueOnFileView.setModel(that.getOwnerComponent().getModel("FileUploadPreviousEmpView"));
				this.getView().addDependent(this._oValueOnFileView);

			}
			this._oValueOnFileView.open();

			// get the list of file from server 
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oPreviousEmpData.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();
			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},
		onStartUpload: function(oEvent) {
			var that = this;
			var oDataModel = this.getView().getModel();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionPreviousEmp");
			var cFiles = oUploadCollection.getItems().length;

			if (cFiles > 0) {
				/*var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
				oUploadCollection.setUploadUrl(sUrl);*/
				oUploadCollection.upload();
				sap.ui.core.BusyIndicator.show();
			}
		},
		onUploadComplete: function(oEvent) {
			var that = this;
			var oPreviousEmpData = this.getView().getModel("PreviousEmployement").getData();
			var oDesc = oPreviousEmpData.buttonValue;
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionPreviousEmp");
			var sResponse = oEvent.mParameters.files[0].response;
			var sStatus = oEvent.mParameters.files[0].status;

			if (sStatus === 201) {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.information("'" + oDesc + "'" + " " + " Document Generated Successfully.");
					oUploadCollection.removeAllAggregation("items");
				// for (var i = 0; i <= oUploadCollection._aFileUploadersForPendingUpload.length; i++) {
				// 	oUploadCollection._aFileUploadersForPendingUpload[i].destroy();
				// }
			} else {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.error(sResponse + " " + " File not Uploaded !");
			}
		},

		onChangeFileUpload: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oDataModel = this.getView().getModel();
			oDataModel.refreshSecurityToken();
			var oHeaders = oDataModel.oHeaders;
			var sToken = oHeaders['x-csrf-token'];

			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: sToken
			});
			// add token as parameter
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},

		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);
		},
		handleEditPress: function() {
			this._toggleButtonsAndView(true);
		},
		handleCancelPress: function() {
			this._toggleButtonsAndView(false);
		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);
		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);
		},
		_onReadPreviousEmployementDataSet: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oModel = this.getOwnerComponent().getModel();
			//var oPernerFilter = new Filter("personnelNo", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			//var oPernerFilter = new sap.ui.model.Filter("personnelNo", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new sap.ui.model.Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/TaxPreviousEmploymentSet";

			oModel.read(oUrl, {
				filters: [oFiscalYearFilter],
				success: function(response) {
					oJsonGlobalData.oTotal80CDeclaredAmount = 0;
					var data = response.results;
					var oJsonSec80cModel = that.getOwnerComponent().getModel('PreviousEmployement');
					oJsonSec80cModel.setData(data);
				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
						' ')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[0]
						.actual === 'X')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				}

				this.byId("editItemId").setVisible(false);
				this.byId("displayItemId").setVisible(true);
				this.byId('save').setVisible(false);
				this.byId('cancel').setVisible(false);
			}.bind(this));
		},
		onChange: function(oEvent) {

			// input field validation
			var dataValid = parseInt(oEvent.getSource().getValue());

			if (dataValid < 0) {
				MessageToast.show("input field can't hold negative value: " + dataValid);
				return oEvent.getSource().setValue("");
			}

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},

		onSave: function() {
			var dataArray = this.getOwnerComponent().getModel("PreviousEmployement").getData();
			for (var i = 0; i < dataArray.length; i++) {
				delete dataArray[i].__metadata;
			}

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			if (oJsonGlobalData.dataValidInt < 0) {
				MessageBox.information("Input Amount can't be negative");
			} else if (oJsonGlobalData.dataValidInt === 0) {
				MessageBox.information("Input Amount can't be zero");
			} else {
				this._onSavePreviousEmployementDataSet(dataArray);
			}

		},

		_onSavePreviousEmployementDataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/TaxPreviousEmploymentSet";
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, '1000');
			//var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, '2022-2023');
			oModel.create(oUrl, oData[0], {
					success: function(response) {
						MessageBox.information("Income from Previous Employement data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		}

	});

});