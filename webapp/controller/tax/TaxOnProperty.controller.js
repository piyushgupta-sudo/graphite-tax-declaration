sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/m/UploadCollectionParameter",
	"com/infocus/fi/tax/ztaxapp/model/formatter"
], function(BaseController, Fragment, MessageBox, MessageToast, Filter, UploadCollectionParameter, formatter) {
	"use strict";

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.TaxOnProperty", {
        formatter : formatter,
		onInit: function() {
			//var oRouter = this.getRouter();

			//	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
			//this._onReadHousePropertyDataSet();
			this._initialDisplay();
			this.getOwnerComponent().getEventBus().subscribe("Default", "getHousePropertyData", () => {
				this._onReadHousePropertyDataSet();
			});
		},
			openFileUpload: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oHousePropertyData = this.getView().getModel("HouseProperty").getData();
			oHousePropertyData.buttonValue = buttonValue;
			this.getView().getModel("HouseProperty").setData(oHousePropertyData);

			if (!this._oValueOnFileUpload) {
				this._oValueOnFileUpload = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileUploadHouseProperty", this);
				this._oValueOnFileUpload.setModel(that.getOwnerComponent().getModel("FileUploadHouseProperty"));
				this.getView().addDependent(this._oValueOnFileUpload);
			}
			this._oValueOnFileUpload.open();
			var btnPreviousEmpViewFile = that.getView().byId("btnPreviousEmpViewFile");
			btnPreviousEmpViewFile.setEnabled(true);
		},
		onStartUpload: function(oEvent) {
			var that = this;
			var oDataModel = this.getView().getModel();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHouseProperty");
			var cFiles = oUploadCollection.getItems().length;

			if (cFiles > 0) {
				/*var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
				oUploadCollection.setUploadUrl(sUrl);*/
				oUploadCollection.upload();
				sap.ui.core.BusyIndicator.show();
			}
		},
		onCancelPressView: function(oEvent) {
			this._oValueOnFileView.close();
				this.byId('save').setVisible(false);
					this.byId('cancel').setVisible(false);
            
		},
		viewUploadedFile: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oHousePropertyData = this.getView().getModel("HouseProperty").getData();
			oHousePropertyData.buttonValue = buttonValue;
			this.getView().getModel("HouseProperty").setData(oHousePropertyData);
			

			// fragment open 
			if (!this._oValueOnFileView) {
				this._oValueOnFileView = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.view.FileUploadHousePropertyView", this);
				this._oValueOnFileView.setModel(that.getOwnerComponent().getModel("FileUploadHousePropertyView"));
				this.getView().addDependent(this._oValueOnFileView);

			}
			this._oValueOnFileView.open();

			// get the list of file from server 
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oHousePropertyData.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();
			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		onUploadComplete: function(oEvent) {
			var that = this;
			var oHousePropertyData = this.getView().getModel("HouseProperty").getData();
			var oDesc = oHousePropertyData.buttonValue;
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHouseProperty");
			var sResponse = oEvent.mParameters.files[0].response;
			var sStatus = oEvent.mParameters.files[0].status;

			if (sStatus === 201) {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.information("'" + oDesc + "'" + " " + " Document Generated Successfully.");
				
				// for (var i = 0; i <= oUploadCollection._aFileUploadersForPendingUpload.length; i++) {
				// 	oUploadCollection._aFileUploadersForPendingUpload[i].destroy();
				// }
				oUploadCollection.removeAllAggregation("items");
			} else {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.error(sResponse + " " + " File not Uploaded !");
			}
		},
		onCancelPress: function(oEvent) {
			this._oValueOnFileUpload.close();
		},
		onBeforeUploadStarts: function(oEvent) {
			var oHousePropertyData = this.getView().getModel("HouseProperty").getData();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHouseProperty");
			var cFiles = oUploadCollection.getItems().length;
			var oComponentName = oHousePropertyData.buttonValue;

			var oValue = [];
			oValue.push(oComponentName);
			oValue.push(oEvent.getParameter("fileName"));

			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: oValue
			});

			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		},
		onChangeFileUpload: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oDataModel = this.getView().getModel();
			oDataModel.refreshSecurityToken();
			var oHeaders = oDataModel.oHeaders;
			var sToken = oHeaders['x-csrf-token'];

			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: sToken
			});
			// add token as parameter
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},

		handleLendersTypeValueHelp: function() {
			var that = this;
			if (!this._oValueHelpLenderTypeDialog) {
				this._oValueHelpLenderTypeDialog = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.LendersType", this);
				//this._oValueHelpAccDialog.setModel(that.getOwnerComponent().getModel("AccType"));
				this.getView().addDependent(this._oValueHelpLenderTypeDialog);
			}
			this._oValueHelpLenderTypeDialog.open();
		},

		handleLendersTypeValueHelpClose: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem"),
				oInput = this.byId("lenderTypeInput");

			if (oSelectedItem) {
				var oValue = oSelectedItem.getTitle() + " " + oSelectedItem.getDescription();
				this.byId("lenderTypeInput").setValue(oValue);
				var oLendersTypeData = this.getOwnerComponent().getModel('HouseProperty').getData();
				oLendersTypeData[0].lendersType = oSelectedItem.getTitle();
				this.getOwnerComponent().getModel('HouseProperty').setData(oLendersTypeData);
			}

			if (!oSelectedItem) {
				oInput.resetProperty("");
			}
		},
		onDialogLendersTypeClose: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				var year = aContexts.map(function(oContext) {
					return oContext.getObject().Title;
				}).join(", ");
				MessageToast.show("You have chosen " + year);

			} else {
				MessageToast.show("No new item was selected.");
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);
		},
		handleEditPress: function() {
			this._toggleButtonsAndView(true);
		},
		handleCancelPress: function() {
			this._toggleButtonsAndView(false);
		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);
		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);
		},
		_onReadHousePropertyDataSet: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oModel = this.getOwnerComponent().getModel();
			//var oPernerFilter = new Filter("personnelNo", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			//var oPernerFilter = new sap.ui.model.Filter("personnelNo", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new sap.ui.model.Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/TaxHousePropertySet";

			oModel.read(oUrl, {
				filters: [oFiscalYearFilter],
				success: function(response) {
					var oJsonGlobalData = that.getOwnerComponent().getModel("globalData").getData();
					oJsonGlobalData.identityNumber = "";
					var data = response.results;

					oJsonGlobalData.identityNumber = data[0].identityNumber;
					var sec24Int = data[0].deductionInterestSec24.trim();
					var lenderTpe = data[0].lendersType.trim();

					data[0].deductionInterestSec24 = sec24Int;
					data[0].lendersType = lenderTpe;

					var oJsonSec80cModel = that.getOwnerComponent().getModel('HouseProperty');
					oJsonSec80cModel.setData(data);

					that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);

				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
						' ')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[0]
						.actual === 'X')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				}

				this.byId("editItemId").setVisible(false);
				this.byId("displayItemId").setVisible(true);
				this.byId('save').setVisible(false);
				this.byId('cancel').setVisible(false);
			}.bind(this));
		},
		onChange: function(oEvent) {

			// input field validation
			var dataValid = parseInt(oEvent.getSource().getValue());

			if (dataValid < 0) {
				MessageToast.show("input field can't hold negative value: " + dataValid);
				return oEvent.getSource().setValue("");
			}

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},
/*		onChangePan: function(oEvent) {
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			//input field validation 
			if (oJsonGlobalData.identityNumber === "") {
				oEvent.getSource().setValue("");
			} else if (oJsonGlobalData.identityNumber) {
				MessageBox.information("PAN no can't be change");
				oEvent.getSource().setValue(oJsonGlobalData.identityNumber);
			}

		},*/

		onSave: function() {
			var dataArray = this.getOwnerComponent().getModel("HouseProperty").getData();
			var chkBox = this.byId("chkBox");
		    
			for (var i = 0; i < dataArray.length; i++) {
				delete dataArray[i].__metadata;
			}
			if(chkBox.mProperties.selected === true){
				dataArray[0].eligibleSection80EEA = "1";
			}
			else{
				dataArray[0].eligibleSection80EEA = "";
			}
			var oJsonSec80cModel = this.getOwnerComponent().getModel('HouseProperty');
			oJsonSec80cModel.setData(dataArray);
			this._onSaveHousePropertyDataSet(dataArray);
		},

		_onSaveHousePropertyDataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/TaxHousePropertySet";
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, '1000');
			//var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, '2022-2023');
			oModel.create(oUrl, oData[0], {
					success: function(response) {
						MessageBox.information("Income from Property data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		}

	});

});