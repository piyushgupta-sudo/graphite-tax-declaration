sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"jquery.sap.global",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/commons/FileUploader",
	"sap/m/UploadCollectionParameter",
	"com/infocus/fi/tax/ztaxapp/model/formatter"
], function(BaseController, jQuery, Fragment, MessageBox, MessageToast, Filter, FileUploader, UploadCollectionParameter, formatter) {
	"use strict";

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.Tax80C", {
        formatter: formatter,
		onInit: function() {
			//	var oRouter = this.getRouter();

			//	oRouter.getRoute("appHome").attachMatched(this._onRouteMatched, this);

			//this._formFragments = {};
			//this._showFormFragment("ChangeTax80C");
			this._initialDisplay();
			// console.log(this._initialDisplay());
			this.getOwnerComponent().getEventBus().subscribe("Default", "get80CData", () => {
				this._onRead80CDataSet();

			});
			//this._onRead80CDataSet();

		},
		onChangeProposed: function(oEvent) {

			var dataValid = parseInt(oEvent.getSource().getValue());

			// negative value not accepted
			/*			if (dataValid < 0) {
							MessageToast.show("input field can't hold negative value: " + dataValid);
							return oEvent.getSource().setValue("");
						}*/

			var oData = this.getOwnerComponent().getModel('sec80c').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			oJsonGlobalData.oTotal80CAmountDeclared = 0;

			// for (var i = 0; i < oData.length; i++) {
				var newData = oData[0];
				var strj;
				for (var j = 1; j <= 30; j++) {
					strj = '';
					if (j < 10) {
						strj = '0' + j.toString();
					} else {
						strj = j.toString();
					}
					var str = "Pin" + strj;
					oJsonGlobalData.oTotal80CAmountDeclared += Number(newData[str]);
				}
			// }

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},
			onLiveChangeActual: function(oEvent) {
			var that = this;
			var dataLiveValid = parseInt(oEvent.getSource().getValue());
			var oData = this.getOwnerComponent().getModel('sec80c').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			// selecting the button ID
			var indexParam = Number(oEvent.mParameters.id.split("-")[2].replace(/[^0-9]/g, "")) - 1;
			var btn80IdArray = [];
			var btn80Id;
			for (var l = 1; l <= 30; l++) {
				btn80Id = "btn80c_" + l.toString();
				btn80IdArray.push(btn80Id);
			}
			var btn80IdActual = btn80IdArray[indexParam];

			// Button disable enable & oTotaldeclaredValue added 
			if (dataLiveValid !== NaN && dataLiveValid > 0) {

				that.byId(btn80IdActual).setEnabled(true);
				oJsonGlobalData.oTotal80CAmountDeclared = 0;

				// for (var i = 0; i < oData.length; i++) {
					var newData = oData[0];
					var strj;
					for (var j = 1; j <= 30; j++) {
						strj = '';
						if (j < 10) {
							strj = '0' + j.toString();
						} else {
							strj = j.toString();
						}
						var str = "Ain" + strj;
						oJsonGlobalData.oTotal80CAmountDeclared += Number(newData[str]);
					}
				// }
				/*alert(oJsonGlobalData.oTotal80CAmountDeclared);*/
				that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);

			} else {
				// that.byId(btn80IdActual).setEnabled(false);
				oJsonGlobalData.oTotal80CAmountDeclared = 0;

				// for (var i = 0; i < oData.length; i++) {
					var newData = oData[0];
					var strj;
					for (var j = 1; j <= 30; j++) {
						strj = '';
						if (j < 10) {
							strj = '0' + j.toString();
						} else {
							strj = j.toString();
						}
						var str = "Ain" + strj;
						oJsonGlobalData.oTotal80CAmountDeclared += Number(newData[str]);
					}
				// }
				/*alert(oJsonGlobalData.oTotal80CAmountDeclared);*/
				that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
			}

			/*(dataLiveValid !== undefined && dataLiveValid > 0) ? that.byId(btn80IdActual).setEnabled(true): (that.byId(btn80IdActual).setEnabled(false) && oEvent.getSource().setValue(""));*/

		},
		onCancelPressView: function(oEvent) {
			this._oValueOnFileView.close();
			this.byId('edit').setVisible(false);

		},
		viewUploadedFile: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");
			var oSec80cData = this.getView().getModel("sec80c").getData();
			oSec80cData.buttonValue = buttonValue;
			this.getView().getModel("sec80c").setData(oSec80cData);
			/*var btn80c = this.getView().byId("btn80c");*/

			// fragment open 
			if (!this._oValueOnFileView) {
				this._oValueOnFileView = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.view.FileUpload80cView", this);
				this._oValueOnFileView.setModel(that.getOwnerComponent().getModel("FileUpload80cView"));
				this.getView().addDependent(this._oValueOnFileView);

			}
			this._oValueOnFileView.open();

			// get the list of file from server 
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oSec80cData.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();
			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},
		onFileDeleted: function(oEvent) {
			var that = this;
			var oModel = that.getOwnerComponent().getModel();
			var fileName = oEvent.mParameters.item.mBindingInfos.fileName.binding.oValue;
			var oDesc = this.getView().getModel("sec80c").getData().buttonValue;
			var oPerner = this.getOwnerComponent().getModel("globalData").getData().userId;
			var sRootUrl = this.getView().getModel().sServiceUrl;
			var viewFileId = this.getView().byId("UploadCollection80cView");
			/*var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oSec80cData.buttonValue);
			var oFileFilter = new sap.ui.model.Filter("Filename", sap.ui.model.FilterOperator.EQ, fileName);*/
			sap.ui.core.BusyIndicator.show();

			/*var sUrl = `/ZfileSet(Pernr='1048',Zdesc='${oDesc}',Filename='${fileName}')/$value`;*/

			var sPath = this.getView().getModel().createKey("/zfileSet", {
				Pernr: oPerner,
				Zdesc: oDesc,
				Filename: fileName
			}) + "/$value";

			/*var sPath = this.getView().getModel().createKey("/ZfileSet(Pernr='1048',Zdesc='Tuition fee - child 1',Filename='BCL.jpg')/$value");*/

			oModel.remove(sPath, {
				method: "DELETE",
				success: function(response) {
					console.log(response);
                    that.uploadedFileSet();
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});
		},
		uploadedFileSet: function(oEvent) {
			var that = this;
			var oSec80cData = this.getView().getModel("sec80c").getData();
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oSec80cData.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();

			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});
		},
	
		

		openFileUpload: function(oEvent) {
			var that = this;
			var btc80cId = oEvent.mParameters.id;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");
			var oSec80cData = this.getView().getModel("sec80c").getData();
			oSec80cData.buttonValue = buttonValue;
			this.getView().getModel("sec80c").setData(oSec80cData);

			if (!this._oValueOnFileUpload) {
				this._oValueOnFileUpload = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileUpload80c", this);
				this._oValueOnFileUpload.setModel(that.getOwnerComponent().getModel("FileUpload80c"));
				this.getView().addDependent(this._oValueOnFileUpload);
			}
			this._oValueOnFileUpload.open();
			var btn80ViewFileString = btc80cId.split("btn80c")[0]+"btn80cViewFile"+btc80cId.split("btn80c")[1];
			/*__xmlview0--btn80cViewFile-__clone54*/
			var btn80cViewFile = that.getView().byId(btn80ViewFileString);
			btn80cViewFile.setEnabled(true);
		},
		
		btnFilesList80c: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oSec80cData = this.getView().getModel("sec80c").getData();
			oSec80cData.buttonValue = buttonValue;
			this.getView().getModel("sec80c").setData(oSec80cData);

			if (!this._oValueOnFileListOpen) {
				this._oValueOnFileListOpen = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileList80c", this);
				this._oValueOnFileListOpen.setModel(that.getOwnerComponent().getModel("FileList80c"));
				this.getView().addDependent(this._oValueOnFileListOpen);
			    this._oValueOnFileUpload.close();
				
			}
			
			this._oValueOnFileListOpen.open();
			
		},
		onCancelPress: function(oEvent) {
			this._oValueOnFileUpload.close();

		},

		onChangeFileUpload: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oDataModel = this.getView().getModel();
			oDataModel.refreshSecurityToken();
			var oHeaders = oDataModel.oHeaders;
			var sToken = oHeaders['x-csrf-token'];

			/*(sToken)? MessageToast.show("") : MessageToast.show("Token is not Available");*/

			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: sToken
			});
			// add token as parameter
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);

		},
		onStartUpload: function(oEvent) {
			var that = this;
			var oDataModel = this.getView().getModel();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollection80c");
			var cFiles = oUploadCollection.getItems().length;

			if (cFiles > 0) {
				var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
				oUploadCollection.setUploadUrl(sUrl);
				oUploadCollection.upload();
			}
		},
		onBeforeUploadStarts: function(oEvent) {
			var oSec80cData = this.getView().getModel("sec80c").getData();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollection80c");
			var cFiles = oUploadCollection.getItems().length;
			var oComponentName = oSec80cData.buttonValue;
			var oValue = [];

			/*if(oComponentName.indexOf(",") != -1){
			    var oElementName = oComponentName.split(",");
			    var newElementName = oElementName[0] + " " + oElementName[1];
			    oValue.push(newElementName);
			    
			}else{
			    oValue.push(oComponentName);
			}*/

			oValue.push(oComponentName);

			// file Name
			oValue.push(oEvent.getParameter("fileName"));
			var file = oEvent.getParameter("fileName");

			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: oValue
			});

			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
			/*setTimeout(function() {
				MessageToast.show("Event beforeUploadStarts triggered");
			}, 2000);*/
		},

		onUploadComplete: function(oEvent) {
		var that = this;
			// response Text
			var oSec80cData = this.getView().getModel("sec80c").getData();
			var oDesc = oSec80cData.buttonValue;
			var oUploadCollection = sap.ui.getCore().byId("UploadCollection80c");
			var sResponse = oEvent.mParameters.files[0].response;
			var sStatus = oEvent.mParameters.files[0].status;

			if (sStatus === 201) {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.information("'" + oDesc + "'" + " " + " Document Generated Successfully.");

			
			 oUploadCollection.removeAllAggregation("items");

			} else {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.error(sResponse + " " + " File not Uploaded !");
			}

			this.getView().getModel().refresh();
			this.getView().getModel("itemsData").refresh();
		},
		onAfterRenderer: function() {

		},
		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);

		},
		handleEditPress: function() {

			//Clone the data
			//this._oSupplier = Object.assign({}, this.getView().getModel().getData().SupplierCollection[0]);
			this._toggleButtonsAndView(true);

		},
		handleCancelPress: function() {

			//Restore the data
			/*var oModel = this.getView().getModel();
			var oData = oModel.getData();

			oData.SupplierCollection[0] = this._oSupplier;

			oModel.setData(oData);*/
			this._toggleButtonsAndView(false);

		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);

		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();

			// Show the appropriate action buttons
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			// Set the right form type
			//this._showFormFragment(bEdit ? "ChangeTax80C" : "DisplayTax80C");
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);

		},

		_showFormFragment: function(sFragmentName) {
			var oPage = this.byId("Page80C");
			oPage.removeAllContent();
			oPage.insertContent(this._getFormFragment(sFragmentName));
		},

		_getFormFragment: function(sFragmentName) {
			var oFormFragment = this._formFragments[sFragmentName];
			if (oFormFragment) {
				return oFormFragment;
			}
			oFormFragment = sap.ui.xmlfragment(this.getView().getId(), "sap.ui.demo.nav.view.employee.tax.tax80c." + sFragmentName);
			this._formFragments[sFragmentName] = oFormFragment;
			return this._formFragments[sFragmentName];
		},
		_onRead80CDataSet: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			var oModel = this.getOwnerComponent().getModel();
			//var oPernerFilter = new Filter("EmpNo", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			//var oPernerFilter = new sap.ui.model.Filter("EmpNo", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("Fiscal", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/Tax_80CSet";

			oModel.read(oUrl, {
				filters: [oFiscalYearFilter],
				success: function(response) {
					var data = response.results;
					console.log(data);
					var oJsonSec80cModel = that.getOwnerComponent().getModel('sec80c');
					oJsonSec80cModel.setData(data);

					var oJsonGlobalData = that.getOwnerComponent().getModel("globalData").getData();
					oJsonGlobalData.oTotal80DeclaredAmount = 0;

					var headerModel = that.getOwnerComponent().getModel('employeeData').getData();
					var total = 0;
					oJsonGlobalData.oTotal80CAmountDeclared = 0;

					// proposed & actual logic based on flag indicater
					if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
							' ')) {

					var newData = data[0];
							var strj;
							for (var j = 1; j <= 30; j++) {
								strj = '';
								if (j < 10) {
									strj = '0' + j.toString();
								} else {
									strj = j.toString();
								}
								var str = "Pin" + strj;
								total += Number(newData[str]);
							}
						
                         oJsonGlobalData.oTotal80CAmountDeclared += total
					} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[
								0]
							.actual === 'X')) {

						
							var newData = data[0];
							var strj;
							for (var j = 1; j <= 30; j++) {
								strj = '';
								if (j < 10) {
									strj = '0' + j.toString();
								} else {
									strj = j.toString();
								}
								var str = "Ain" + strj;
								total += Number(newData[str]);
							}
						
                         oJsonGlobalData.oTotal80CAmountDeclared += total
					}

					that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);

				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				var oIdProposed;
				var oIdActual;
				var btn80Id;
				if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
						' ')) {
					this.byId('edit').setEnabled(true);
						this.byId('edit').setVisible(true);

						for (var i = 1; i <= 30; i++) {
							oIdProposed = "editItemProposed" + i.toString();
							this.byId(oIdProposed).setEnabled(true);
						}

						for (var j = 1; j <= 30; j++) {
							oIdActual = "editItemActual" + j.toString();
							this.byId(oIdActual).setEnabled(false);
						}
							
						for (var k = 1; k <= 30; k++) {
						btn80Id = "btn80c_" + k.toString();
						this.byId(btn80Id).setEnabled(false);
					}
						// for (var k = 1; k <= 40; k++) {
						// btn80Id = "btn80_" + k.toString();
						// this.byId(btn80Id).setEnabled(false);
					// }
					// this.byId("editItemProposed80C").setEnabled(true);
					// this.byId("editItemActual80C").setEnabled(false);

					/*this.byId("editItemId").setVisible(false);
					this.byId("displayItemId").setVisible(true);*/
				} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[0]
						.actual === 'X')) {
					this.byId('edit').setEnabled(true);
						this.byId('edit').setVisible(true);

						for (var i = 1; i <= 30; i++) {
							oIdProposed = "editItemProposed" + i.toString();
							this.byId(oIdProposed).setEnabled(false);
						}

						for (var j = 1; j <= 30; j++) {
							oIdActual = "editItemActual" + j.toString();
							this.byId(oIdActual).setEnabled(true);
						}
						
						for (var k = 1; k <= 30; k++) {
						btn80Id = "btn80c_" + k.toString();
						this.byId(btn80Id).setEnabled(false);
					}
					// 	for (var k = 1; k <= 40; k++) {
					// 	btn80Id = "btn80_" + k.toString();
					// 	this.byId(btn80Id).setEnabled(false);
					// }
					// this.byId("editItemProposed80C").setEnabled(false);
					// this.byId("editItemActual80C").setEnabled(true);

				} else {
				this.byId('edit').setEnabled(false);
					this.byId('edit').setVisible(false);
					this.byId('save').setVisible(false);
					this.byId('cancel').setVisible(false);
					this.byId("editItemId").setVisible(false)
					this.byId("displayItemId").setVisible(true);

				}

				/*this.byId("editItemId").setVisible(false);
				this.byId("displayItemId").setVisible(true);*/
			}.bind(this));
		},
		onChange: function(oEvent) {

			var dataValid = parseInt(oEvent.getSource().getValue());

			// negative value not accepted
			/*			if (dataValid < 0) {
							MessageToast.show("input field can't hold negative value: " + dataValid);
							return oEvent.getSource().setValue("");
						}*/

			var oData = this.getOwnerComponent().getModel('sec80c').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			oJsonGlobalData.oTotal80CAmountDeclared = 0;
			for (var i = 0; i < oData.length; i++) {
				oJsonGlobalData.oTotal80CAmountDeclared += Number(oData[i].PcnAmount);
			}

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
			//this.totalDeclaredAmount += Number(oEvent.getSource().getValue());
		},

		handleUploadComplete: function(oEvent) {
			var sResponse = oEvent.getParameter("response");
			if (sResponse) {
				var sMsg = "";
				var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
				if (m[1] == "200") {
					sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Success)";
					oEvent.getSource().setValue("");
				} else {
					sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Error)";
				}

				MessageToast.show(sMsg);
				/*this.setAttachmentModel();*/
			}
		},
		/*		onUploadFile: function(oEvent) {
					var oFileUploader = this.getView().byId("fileUploaderFS");
					var sFile = oFileUploader.getValue();
					console.log(sFile);

					//var domRef = oFileUploader.getFocusDomRef();
					//var file = domRef.files[0];

					//check file has been entered

					if (!sFile) {
						var sMsg = "Please select a file first";
						sap.m.MessageToast.show(sMsg);
						return;
					} else {
						var that = this;
						oFileUploader.upload();

					}

				},*/

		onSave: function() {
			var dataArray = this.getOwnerComponent().getModel('sec80c').getData();
			for (var i = 0; i < dataArray.length; i++) {
				delete dataArray[i].__metadata;
			}
			dataArray = JSON.parse(JSON.stringify(dataArray[0]));
			console.log("Data Array: "+dataArray);

			// var odata = {
				
			// 	"ToEmp": dataArray
			// };

			// validation logic
			// var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			// if (oJsonGlobalData.dataValidInt < 0) {
			// 	MessageBox.information("Input Amount can't be negative");
			// } else if (oJsonGlobalData.dataValidInt === 0) {
			// 	MessageBox.information("Input Amount can't be zero");
			// } else if (oJsonGlobalData.oTotal80CAmountDeclared < 0) {
			// 	MessageBox.information("Total Amount can't be negative");
			// } else if (oJsonGlobalData.oTotal80CAmountDeclared === 0) {
			// 	MessageBox.information("Total Amount can't be Zero");
			// } else {
			// 	this._onSave80CDataSet(odata);
			// }
			this._onSave80CDataSet(dataArray);

		},

		_onSave80CDataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/Tax_80CSet";
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, '1000');
			//var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, '2022-2023');
			oModel.create(oUrl, oData, {
					success: function(response) {
						MessageBox.information("The 80c data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		}

	});

});