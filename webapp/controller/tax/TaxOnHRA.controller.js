sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/m/UploadCollectionParameter",
		"com/infocus/fi/tax/ztaxapp/model/formatter"
], function(BaseController, Fragment, MessageBox, MessageToast, Filter, UploadCollectionParameter, formatter) {
	"use strict";

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.TaxOnHRA", {
		formatter : formatter,
		_oValueHelpCityDialog: null,
		_oValueHelpAccDialog: null,
		onInit: function() {
			//var oRouter = this.getRouter();

			//	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
			//this._onReadHRADataSet();
			this._initialDisplay();
			this._getAccomodationType();
			this.getOwnerComponent().getEventBus().subscribe("Default", "getHRAData", () => {
				this._onReadHRADataSet();
			});
		},
		openFileUpload: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oHRAData = this.getView().getModel("HRA").getData();
			oHRAData.buttonValue = buttonValue;
			this.getView().getModel("HRA").setData(oHRAData);

			if (!this._oValueOnFileUpload) {
				this._oValueOnFileUpload = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileUploadHRA", this);
				this._oValueOnFileUpload.setModel(that.getOwnerComponent().getModel("FileUploadHRA"));
				this.getView().addDependent(this._oValueOnFileUpload);
			}
			this._oValueOnFileUpload.open();
			var btnHRAViewFile = that.getView().byId("btnHRAViewFile");
			btnHRAViewFile.setEnabled(true);
		},
		onChangeFileUpload: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oDataModel = this.getView().getModel();
			oDataModel.refreshSecurityToken();
			var oHeaders = oDataModel.oHeaders;
			var sToken = oHeaders['x-csrf-token'];

			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: sToken
			});
			// add token as parameter
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},
		onStartUpload: function(oEvent) {
			var that = this;
			var oDataModel = this.getView().getModel();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHRA");
			var cFiles = oUploadCollection.getItems().length;

			if (cFiles > 0) {
				/*var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
				oUploadCollection.setUploadUrl(sUrl);*/
				oUploadCollection.upload();
				sap.ui.core.BusyIndicator.show();
			}
		},
		onBeforeUploadStarts: function(oEvent) {
			var oHRAData = this.getView().getModel("HRA").getData();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHRA");
			var cFiles = oUploadCollection.getItems().length;
			var oComponentName = oHRAData.buttonValue;

			var oValue = [];
			oValue.push(oComponentName);
			oValue.push(oEvent.getParameter("fileName"));

			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: oValue
			});

			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		},
		onCancelPress: function(oEvent) {
			this._oValueOnFileUpload.close();

		},
		viewUploadedFile: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");

			var oHRAData = this.getView().getModel("HRA").getData();
			oHRAData.buttonValue = buttonValue;
			this.getView().getModel("HRA").setData(oHRAData);
			

			// fragment open 
			if (!this._oValueOnFileView) {
				this._oValueOnFileView = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.view.FileUploadHRAView", this);
				this._oValueOnFileView.setModel(that.getOwnerComponent().getModel("FileUploadHRAView"));
				this.getView().addDependent(this._oValueOnFileView);

			}
			this._oValueOnFileView.open();

			// get the list of file from server 
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oHRAData.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();
			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},
		onCancelPressView: function(oEvent) {
			this._oValueOnFileView.close();
			this.byId('save').setVisible(false);
			this.byId('cancel').setVisible(false);

		},

		onUploadComplete: function(oEvent) {
			var that = this;
			var oHRAData = this.getView().getModel("HRA").getData();
			var oDesc = oHRAData.buttonValue;
			var oUploadCollection = sap.ui.getCore().byId("UploadCollectionHRA");
			var sResponse = oEvent.mParameters.files[0].response;
			var sStatus = oEvent.mParameters.files[0].status;

			if (sStatus === 201) {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.information("'" + oDesc + "'" + " " + " Document Generated Successfully.");

			oUploadCollection.removeAllAggregation("items");
			} else {
				that.onCancelPress();
				sap.ui.core.BusyIndicator.hide();
				MessageBox.error(sResponse + " " + " File not Uploaded !");
			}
		},
		handleAccValueHelp: function() {
			var that = this;
			if (!this._oValueHelpAccDialog) {
				this._oValueHelpAccDialog = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.AccomodationDialog", this);
				this._oValueHelpAccDialog.setModel(that.getOwnerComponent().getModel("AccType"));
				this.getView().addDependent(this._oValueHelpAccDialog);
			}
			this._oValueHelpAccDialog.open();
		},

		handleAccValueHelpClose: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem"),
				oInput = this.byId("accInput");

			if (oSelectedItem) {
				var oValue = oSelectedItem.getTitle() + " " + oSelectedItem.getDescription();
				//this.byId("accInput").setValue(oValue);
				var oHouseTypeData = this.getOwnerComponent().getModel('HRA').getData();
				oHouseTypeData[0].housingType = oValue;
				this.getOwnerComponent().getModel('HRA').setData(oHouseTypeData);
			}

			if (!oSelectedItem) {
				oInput.resetProperty("");
			}
		},
		onDialogAccClose: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				var year = aContexts.map(function(oContext) {
					return oContext.getObject().Title;
				}).join(", ");
				MessageToast.show("You have chosen " + year);

			} else {
				MessageToast.show("No new item was selected.");
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		handleCityValueHelp: function() {
			if (!this._oValueHelpCityDialog) {
				this._oValueHelpCityDialog = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.CityType", this);
				this.getView().addDependent(this._oValueHelpCityDialog);
			}
			this._oValueHelpCityDialog.open();
		},

		handleCityValueHelpClose: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem"),
				oInput = this.byId("cityInput");

			if (oSelectedItem) {
				var oValue = oSelectedItem.getTitle() + " " + oSelectedItem.getDescription();
				//this.byId("cityInput").setValue(oSelectedItem.getDescription());
				var oCityTypeData = this.getOwnerComponent().getModel('HRA').getData();
				oCityTypeData[0].cityCategory = oValue;
				this.getOwnerComponent().getModel('HRA').setData(oCityTypeData);
			}

			if (!oSelectedItem) {
				oInput.resetProperty("");
			}
		},
		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);
		},
		handleEditPress: function() {
			this._toggleButtonsAndView(true);
		},
		handleCancelPress: function() {
			this._toggleButtonsAndView(false);
		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);
		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);
		},
		_getAccomodationType: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oModel = this.getOwnerComponent().getModel();
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, "1000");

			var oUrl = "/F4_housing_typeSet";

			oModel.read(oUrl, {
				//filters: [oPernerFilter],
				success: function(response) {
					var data = response.results;
					var oJsonSec80cModel = that.getOwnerComponent().getModel('AccType');
					oJsonSec80cModel.setData(data);
				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Accomodation Type" + error);
				}
			});
		},
		_onReadHRADataSet: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oModel = this.getOwnerComponent().getModel();
			//var oPernerFilter = new Filter("personnelNo", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			//var oPernerFilter = new sap.ui.model.Filter("personnelNo", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new sap.ui.model.Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/TaxHouseRentAllowanceSet";

			oModel.read(oUrl, {
				filters: [oFiscalYearFilter],
				success: function(response) {
					var data = response.results;
					var oJsonSec80cModel = that.getOwnerComponent().getModel('HRA');

					if (data[0].HRATaxExempt === "1") {
						data[0].HRATaxExempt = true;
					} else {

						data[0].HRATaxExempt = false;
					}
                    
                    var rentAmount = data[0].rentAmount.trim();
                    data[0].rentAmount = rentAmount;
                    
					oJsonSec80cModel.setData(data);
				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
						' ')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[0]
						.actual === 'X')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);
					this.byId('save').setVisible(true);
					this.byId('cancel').setVisible(true);
				}

				this.byId("editItemId").setVisible(false);
				this.byId("displayItemId").setVisible(true);
				this.byId('save').setVisible(false);
				this.byId('cancel').setVisible(false);
			}.bind(this));
		},
		onChange: function(oEvent) {

			var dataValid = parseInt(oEvent.getSource().getValue());
			// negative value not accepted
			if (dataValid < 0) {
				MessageToast.show("input field can't hold negative value: " + dataValid);
				return oEvent.getSource().setValue("");
			}

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},

		onSave: function() {
			var dataArray = this.getOwnerComponent().getModel("HRA").getData();
			var chkBox = this.byId("chkBox");

			// if (dataArray[0].HRATaxExempt === true) {
			// 	dataArray[0].HRATaxExempt = "1";
			// } else {
			// 	dataArray[0].HRATaxExempt = "";
			// }

			for (var i = 0; i < dataArray.length; i++) {
				delete dataArray[i].__metadata;
			}
			if(chkBox.mProperties.selected === true){
				dataArray[0].HRATaxExempt = "1";
			}
			else{
				dataArray[0].HRATaxExempt = "";
			}
			var oJsonSec80cModel = this.getOwnerComponent().getModel('HRA');
			oJsonSec80cModel.setData(dataArray);
			this._onSaveHRADataSet(dataArray);
		},

		_onSaveHRADataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/TaxHouseRentAllowanceSet";
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, '1000');
			//var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, '2022-2023');
			oModel.create(oUrl, oData[0], {
					success: function(response) {
						MessageBox.information("House Rent Allowance data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		}

	});

});