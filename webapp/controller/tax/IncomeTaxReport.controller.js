var html2pdf;

sap.ui.define(
  [
    "com/infocus/fi/tax/ztaxapp/controller/BaseController",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel",
    "com/infocus/fi/tax/ztaxapp/libs/html2pdf.bundle",
  ],
  function (
    BaseController,
    Fragment,
    MessageBox,
    MessageToast,
    Filter,
    FilterOperator,
    JSONModel,
    com
  ) {
    "use strict";

    return BaseController.extend(
      "com.infocus.fi.tax.ztaxapp.controller.tax.IncomeTaxReport",
      {
        onInit: function () {
          var btn = document.getElementById("btn");
          btn.addEventListener("click", () => {
            this.onClickPdfView();
          });

          //this._initialDisplay();
          this.getOwnerComponent()
            .getEventBus()
            .subscribe("Default", "getIncomeTaxReport", () => {
              this.__onReadActualDataSet();
              this.__onReadProposedDataSet();
              this.__onReadProjectedDataSet();
            });
        },
       /* _initialDisplay: function () {
          document.getElementById("form").style.display = "none";
          document.getElementById("btn").style.display = "none";
          document.getElementById("error_box").style.display = "none";
          document.getElementById("span").style.display = "none";
        },*/

        onDialogAccClose: function (oEvent) {
          var aContexts = oEvent.getParameter("selectedContexts");
          if (aContexts && aContexts.length) {
            var year = aContexts
              .map(function (oContext) {
                return oContext.getObject().Title;
              })
              .join(", ");

            MessageToast.show("You have chosen " + year);
          } else {
            MessageToast.show("No new item was selected.");
          }
          oEvent.getSource().getBinding("items").filter([]);
        },

        _onRouteMatched: function (oEvent) {
          var oArgs, oView;
          oArgs = oEvent.getParameter("arguments");
          oView = this.getView();

          oView.bindElement({
            path: "/Employees(" + oArgs.employeeId + ")",
            events: {
              change: this._onBindingChange.bind(this),
              dataRequested: function (oEvent) {
                oView.setBusy(true);
              },
              dataReceived: function (oEvent) {
                oView.setBusy(false);
              },
            },
          });
        },
        __onReadActualDataSet: function (oEvent) {
          var oJsonGlobalData = this.getOwnerComponent()
            .getModel("globalData")
            .getData();
          /*console.log(oJsonGlobalData);*/
          var selectedYear = oJsonGlobalData.selectedYear;
          var userId = oJsonGlobalData.userId;
          console.log(selectedYear, userId);

          var sModel = this.getOwnerComponent().getModel();
          /*console.log(sModel);*/
          var that = this;
          var oPernerFilter = new sap.ui.model.Filter(
            "pernr",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.userId
          );
          var oFiscalYearFilter = new sap.ui.model.Filter(
            "fiscal",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.selectedYear
          );
          var oUrl = "/ZHREARNINGS_ACTUALSet";

          sModel.read(oUrl, {
            filters: [oPernerFilter, oFiscalYearFilter],
            success: function (response) {
              var data = response.results;
              /*console.log(data);*/

              var actualSetModel = that
                .getOwnerComponent()
                .getModel("oActualSet");
              actualSetModel.setData(data);

              /*console.log(actualSetModel);*/
            },
            error: function (error) {
              console.log(error);
            },
          });

          /*			oModel.attachRequestCompleted(function() {
							var headerData = that.getOwnerComponent().getModel('employeeData').getData();

							if (headerData[0].lender === "X") {
								this.byId("oPDF").setVisible(true);
							} else {
								this.byId("oPDF").setVisible(false);

							}
						}.bind(this));*/
        },
        __onReadProposedDataSet: function (oEvent) {
          var oJsonGlobalData = this.getOwnerComponent()
            .getModel("globalData")
            .getData();
          /*console.log(oJsonGlobalData);*/
          var selectedYear = oJsonGlobalData.selectedYear;
          var userId = oJsonGlobalData.userId;
          /*console.log(selectedYear, userId);*/

          var sModel = this.getOwnerComponent().getModel();
          /*console.log(sModel);*/
          var that = this;
          var oPernerFilter = new sap.ui.model.Filter(
            "pernr",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.userId
          );
          var oFiscalYearFilter = new sap.ui.model.Filter(
            "fiscal",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.selectedYear
          );
          var oUrl = "/ZHREARNINGS_PROPOSEDSet";

          sModel.read(oUrl, {
            filters: [oPernerFilter, oFiscalYearFilter],
            success: function (response) {
              var data = response.results;
              /*console.log(data);*/

              var proposedSetModel = that
                .getOwnerComponent()
                .getModel("oProposedSet");
              proposedSetModel.setData(data);

              /*console.log(proposedSetModel);*/
            },
            error: function (error) {
              console.log(error);
            },
          });

          /*			oModel.attachRequestCompleted(function() {
							var headerData = that.getOwnerComponent().getModel('employeeData').getData();

							if (headerData[0].lender === "X") {
								this.byId("oPDF").setVisible(true);
							} else {
								this.byId("oPDF").setVisible(false);

							}
						}.bind(this));*/
        },
        __onReadProjectedDataSet: function (oEvent) {
          var oJsonGlobalData = this.getOwnerComponent()
            .getModel("globalData")
            .getData();
          /*console.log(oJsonGlobalData);*/
          var selectedYear = oJsonGlobalData.selectedYear;
          var userId = oJsonGlobalData.userId;
          /*console.log(selectedYear, userId);*/

          var pModel = this.getOwnerComponent().getModel();
          /*console.log(sModel);*/
          var that = this;
          var oPernerFilter = new sap.ui.model.Filter(
            "pernr",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.userId
          );
          var oFiscalYearFilter = new sap.ui.model.Filter(
            "fiscal",
            sap.ui.model.FilterOperator.EQ,
            oJsonGlobalData.selectedYear
          );
          var oUrl = "/ZHR_PROJECTSet";

          pModel.read(oUrl, {
            filters: [oPernerFilter, oFiscalYearFilter],
            success: function (response) {
              var data = response.results;
              /*console.log(data);*/

              // Form Model
              var actualSetModel = that
                .getOwnerComponent()
                .getModel("oActualSet");
              var proposedSetModel = that
                .getOwnerComponent()
                .getModel("oProposedSet");
              var projectedSetModel = that
                .getOwnerComponent()
                .getModel("oProjectedSet");
              projectedSetModel.setData(data);

              /*console.log(projectedSetModel);*/

              // All Form Data
              var oActualData = actualSetModel.getData();
              var oProposedData = proposedSetModel.getData();
              var oProjectedData = projectedSetModel.getData();

              var imagePath = "../img/Graphite_Logo.jpg";
              oActualData.imagePathActual = imagePath;

              var newDedsS24 = oActualData[0].DedsS24;
              console.log(newDedsS24);

              oActualData[0].DedsS24 = newDedsS24.replaceAll("-", "");

              actualSetModel.setData(oActualData);

              console.log(oActualData);
              console.log(oProposedData);
              console.log(oProjectedData);

              var pageHeader = document.getElementById("span");
              console.log(pageHeader);
              pageHeader.innerText = `INCOME TAX PROJECTION FOR YEAR ${oActualData[0].fiscal}`;

              document.getElementById("form").innerHTML = `
            			<table>
            
                        <thead>
                            <tr>
                                <td><img src=${oActualData.imagePathActual} alt="image" style="background-size: contain;max-width:70%; height:auto;"></td>
                                <td colspan="2">Graphite India Limited<br>
                                    Income Tax Projection For Year ${oActualData[0].fiscal}</td>
                                <td></td>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td >Employee Name:</td>
                                <td>${oActualData[0].EmpName}</td>
                                <td>Employee code:</td>
                                <td colspan="4">${oActualData[0].EmpCode}</td>
                            </tr>
                            <tr>
                                <td>Department:</td>
                                <td>${oActualData[0].EmpDep}</td>
                                <td>Designation:</td>
                                <td colspan="4">${oActualData[0].EmpDesig}</td>
                            </tr>
                            <tr>
                                <td>Pan no:</td>
                                <td>${oActualData[0].PanNo}</td>
                                <td>Date:</td>
                                <td colspan="4">${oActualData[0].Date}</td>
                            </tr>
                        </tbody>
            
                       
                        <tr>
                            <th>1) Earnings</th>
                            <th class="center">Monthly</th>
                            <th class="center">Cumulated</th>
                            <th class="center">Projected</th>
                            <th class="center">Total</th>
                        </tr>
                        <tr class="empData">
                            <td>Basic Salary</td>
                            <td class="right">${oActualData[0].BasicSalary}</td>
                            <td class="right">${oProposedData[0].BasicSalary}</td>
                            <td class="right">${oProjectedData[0].BasicSalary}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Special Allowance</td>
                            <td class="right">${oActualData[0].SpecialAllowance}</td>
                            <td class="right">${oProposedData[0].SpecialAllowance}</td>
                            <td class="right">${oProjectedData[0].SpecialAllowance}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Adhoc Allowance</td>
                            <td class="right">${oActualData[0].AdhocAllow}</td>
                            <td class="right">${oProposedData[0].AdhocAllow}</td>
                            <td class="right">${oProjectedData[0].AdhocAllow}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>House Rent Allowance</td>
                            <td class="right">${oActualData[0].HouseRent}</td>
                            <td class="right">${oProposedData[0].HouseRent}</td>
                            <td class="right">${oProjectedData[0].HouseRent}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Conveyance Allowance</td>
                            <td class="right">${oActualData[0].Conveyance}</td>
                            <td class="right">${oProposedData[0].Conveyance}</td>
                            <td class="right">${oProjectedData[0].Conveyance}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Personal Allowance</td>
                            <td class="right">${oActualData[0].Personal}</td>
                            <td class="right">${oProposedData[0].Personal}</td>
                            <td class="right">${oProjectedData[0].Personal}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>House Rent Allowance(%)</td>
                            <td class="right">${oActualData[0].HouseRentper}</td>
                            <td class="right">${oProposedData[0].HouseRentper}</td>
                            <td class="right">${oProjectedData[0].HouseRentper}</td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">Monthly Regular Income</td>
                            <td class="right">${oActualData[0].MonthlyIncome}</td>
                            <td class="right">${oProposedData[0].MonthlyIncome}</td>
                            <td class="right">${oProjectedData[0].MonthlyIncome}</td>
                            <td class="right">${oActualData[0].TotalAllowance}</td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td>Educational Allowance</td>
                            <td class="right">${oActualData[0].EducationAllowance}</td>
                            <td class="right">${oProposedData[0].EducationAllowance}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Leave Travel Allowance</td>
                            <td class="right">${oActualData[0].TravelAllowance}</td>
                            <td class="right">${oProposedData[0].TravelAllowance}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Leave Encashment</td>
                            <td class="right">${oActualData[0].LeaveEncashment}</td>
                            <td class="right">${oProposedData[0].LeaveEncashment}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Soft Furnishing</td>
                            <td class="right">${oActualData[0].SoftFurnishing}</td>
                            <td class="right">${oProposedData[0].SoftFurnishing}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Leave Travel Allowance</td>
                            <td class="right">${oActualData[0].LeaveTravelAllowance}</td>
                            <td class="right">${oProposedData[0].LeaveTravelAllowance}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Child Education Scholarship</td>
                            <td class="right">${oActualData[0].EducationSchlrsip}</td>
                            <td class="right">${oProposedData[0].EducationSchlrsip}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Medical</td>
                            <td class="right">${oActualData[0].Medical}</td>
                            <td class="right">${oProposedData[0].Medical}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">Monthly Irregular Income</td>
                            <td class="right">${oActualData[0].MonthlyIrIncome}</td>
                            <td class="right">${oProposedData[0].MonthlyIrIncome}</td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TotalAllowance2}</td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">2) Value of Prequisites</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td>Electricity (Perks)</td>
                            <td class="right"></td>
                            <td class="right">${oProposedData[0].Electricity}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Water (Perks)</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Gas (Perks)</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Annual Perk</td>
                            <td class="right"></td>
                            <td class="right">${oProposedData[0].AnnualPerk}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td>Previous Gross Salary</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Value of Persk u/s 17(2)</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Profit wrt Salary u/s 17(3)</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Exemption under provision to section</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">3) Gross Salary</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].GrossSalary}</td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">4) Exemption U/S 10</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData" id="pageBreak">
                            <td>Conveyance Annual Exemption</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        
                        <tr class="empData" id="repeat">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td>HRA Annual Exemption</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>LTA Annual Exemption</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Total</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">5) Balance</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].Balance}</td>
                        </tr>
                        <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">6) Aggregate of deductions</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Standard Deduction</td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].StandardDeduction}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Professional Tax</td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].ProfessionalTax}</td>
                            <td class="right"></td>
                            <td class="right"></td>
                        </tr>
                        <tr class="empData">
                            <td>Total</td>
                            <td class="right"></td>
                            <td class="right">${oProposedData[0].Total2}</td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].Total2}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">7) Incm under Hd Salary</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].HdSalary}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">8) Deds S24 (Maximum Limit)</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].DedsS24}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">9) Gross Total Income</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <tdclass="right"></td>
                            <td class="right">${oActualData[0].GrossTotal}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">10) Aggregate of Chapter VI</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].AggregateChapter}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">11) Total Income</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TotalIncome}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">12) Tax on Total Income</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TaxOnTotal}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">13) Tax Payable</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TaxPayable}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">14) Health and Education Cess</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].Health}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">15) Tax payable and surcharge</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].Surcharge}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">16) Tax deducted so far</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TaxDeducted}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">17) Net tax payable</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].NetTax}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">18) Income Tax</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].IncomeTax}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">19) Total Tax deductible</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].TotalTax}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">20) Monthly Health & Edu Cess</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].MonthlyHealth}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">21) Monthly Amount Paid</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].MonthlyAmount}</td>
                        </tr>
                        <tr class="empData">
                            <td class="bold">22) Monthly Tax payable</td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right"></td>
                            <td class="right">${oActualData[0].MonthlyTax}</td>
                        </tr>
                            <tr class="empData">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td> 
                            <td></td>
                        </tr>
                    </table>
            					
            		`;
            },
            error: function (error) {
              console.log(error);
            },
          });

          var oModel = this.getOwnerComponent().getModel();
          oModel.attachRequestCompleted(
            function () {
              var headerData = this.getOwnerComponent()
                .getModel("employeeData")
                .getData();

              /*console.log(headerData[0]);*/
              if (headerData[0].lender === "X") {
                document.getElementById("form").style.display = "block";
                document.getElementById("btn").style.display = "block";
                document.getElementById("error_box").style.display = "none";
                document.getElementById("span").style.display = "block";
              } else {
                document.getElementById("form").style.display = "none";
                document.getElementById("btn").style.display = "none";
                document.getElementById("error_box").style.display = "block";
                document.getElementById("span").style.display = "none";

                /*document.getElementById("error_box").innerText = "Year Should be Current Fiscal Year";*/

                var closeBtn = document.getElementById("btn_close");
                closeBtn.addEventListener("click", () => {
                  document.getElementById("error_box").style.display = "none";
                });
              }
            }.bind(this)
          );
        },

        onClickPdfView: function () {
          var that = this;
          var element = document.getElementById("form");
          /*var header = document.getElementById("headerSet");*/
          /*html2pdf(element);*/

          // Form Model
          var actualSetModel = that.getOwnerComponent().getModel("oActualSet");
          // All Form Data
          var oActualData = actualSetModel.getData();
          console.log(oActualData);

          var fileDate = oActualData[0].Date;

          var fileDateNew = fileDate.replaceAll(".", "_");
          console.log(fileDateNew);

          var fileName = oActualData[0].pernr + "_" + fileDateNew;
          console.log(fileName);

          var opt = {
            margin: [10, 10, 30, 10], //top, left, buttom, right,
            filename: fileName,
            image: {
              type: "jpeg",
              quality: 0.98,
            },
            html2canvas: {
              dpi: 192,
              scale: 5,
              letterRendering: true,
            },
            pagebreak: {
              mode: "css",
              after: "#nextPage",
            },
            jsPDF: {
              unit: "pt",
              format: "a4",
              orientation: "portrait",
            },
          };

          /*			var repeat = document.getElementById("repeat");
						console.log(repeat);

						// Form Model
						var actualSetModel = that.getOwnerComponent().getModel("oActualSet");
						// All Form Data
						var oActualData = actualSetModel.getData();
						console.log(oActualData);

						repeat.innerHTML =
							`   <table>
			                        <thead>
			                            <tr>
			                                <td><img src="../img/BCL.jpg" alt="image" style="background-size: contain;max-width:70%; height:auto;"></td>
			                                <td colspan="2">Birla Corporation Limited<br>
			                                    Income Tax Projection For Year ${oActualData[0].fiscal}</td>
			                                <td>Page no 1.</td>
			                            </tr>
			                        </thead>
			                        
			                        <tbody>
			                            <tr class="emp">
			                                <td >Employee Name:</td>
			                                <td>${oActualData[0].EmpName}</td>
			                                <td>Employee code:</td>
			                                <td colspan="4">${oActualData[0]}</td>
			                            </tr>
			                            <tr class="emp">
			                                <td>Department:</td>
			                                <td>${oActualData[0].EmpDep}</td>
			                                <td>Designation:</td>
			                                <td colspan="4">${oActualData[0].EmpDesig}</td>
			                            </tr>
			                            <tr class="emp">
			                                <td>Pan no:</td>
			                                <td>${oActualData[0].PanNo}</td>
			                                <td>Date:</td>
			                                <td colspan="4">${oActualData[0].Date}</td>
			                            </tr>
			                        </tbody> 
						        </table>
						`;*/

          html2pdf()
            .from(element)
            .set(opt)
            .toPdf()
            .get("pdf")
            .then((pdf) => {
              console.log(pdf);
              var totalPages = pdf.internal.getNumberOfPages();
              console.log(totalPages);
              // Form Model
              var actualSetModel = that
                .getOwnerComponent()
                .getModel("oActualSet");
              // All Form Data
              var oActualData = actualSetModel.getData();
              console.log(oActualData);

              /*debugger;*/
              for (var i = 1; i <= totalPages; i++) {
                pdf.setPage(i);
                pdf.setFontSize(11);
                pdf.setTextColor(100);
                pdf.text(
                  "Page " + i + " of " + totalPages,
                  pdf.internal.pageSize.getWidth() / 2.25,
                  pdf.internal.pageSize.getHeight() - 8
                );
              }
              /*document.getElementById("repeatHeader").style.display = "block";*/
            })
            .save();
          /*this.repeatHeaderClose();*/
        },
        repeatHeaderClose: function () {
          var repeat = document.getElementById("repeat");
          console.log(repeat);
          repeat.style.display = "none";
        },
      }
    );
  }
);