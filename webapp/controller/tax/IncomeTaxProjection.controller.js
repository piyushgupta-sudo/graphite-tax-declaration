var html2pdf;

sap.ui.define([
	"com/birla/fi/tax/ztaxapp/controller/BaseController",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/json/JSONModel"
], function(BaseController, Fragment, MessageBox, MessageToast, Filter, FilterOperator, JSONModel) {
	"use strict";

	return BaseController.extend("com.birla.fi.tax.ztaxapp.controller.tax.IncomeTaxProjection", {

		onInit: function() {
		    
		    var btn = document.getElementById("btn");
		    btn.addEventListener("click",()=>{
		        this.onClickPdfView();
		    })

/*			this._initialDisplay();

			this.getOwnerComponent().getEventBus().subscribe("Default", "getIncomeTaxProjection", () => {
				this.__onReadIncomeTaxProjectionDataSet();
			});*/

		},
		_initialDisplay: function() {
			this.byId("oPDF").setVisible(false);
		},

		onDialogAccClose: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				var year = aContexts.map(function(oContext) {
					return oContext.getObject().Title;
				}).join(", ");
				
				MessageToast.show("You have chosen " + year);

			} else {
				MessageToast.show("No new item was selected.");
			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		__onReadIncomeTaxProjectionDataSet: function(oEvent) {

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			console.log(oJsonGlobalData);
			var selectedYear = oJsonGlobalData.selectedYear;
			var userId = oJsonGlobalData.userId;
			console.log(selectedYear, userId);

			// Pdf file view 
			var sUrl = `/sap/opu/odata/sap/ZTAX_PLANNER_SRV/emp_detailsSet(pernr='${userId}',fiscal='${selectedYear}')/$value`;
			var that = this;
			this.getView().byId("oPDF").setSource(sUrl);
		
/*			var incomeTaxProjectionModel = this.getOwnerComponent().getModel('IncomeTaxProjection');
			let oData = {
				"oUrl": [{
					"sUrl": sUrl,
					"height": "1000px",
					"title": "Tax Projection Report"
				}]
			};
			incomeTaxProjectionModel.setData(oData);*/
			/*console.log(incomeTaxProjectionModel);*/

			
			var oModel = this.getOwnerComponent().getModel();

			oModel.attachRequestCompleted(function() {
				var headerData = that.getOwnerComponent().getModel('employeeData').getData();

				if (headerData[0].lender === "X") {
					this.byId("oPDF").setVisible(true);
				} else {
					this.byId("oPDF").setVisible(false);
					
				}
			}.bind(this));
		},
	   onClickPdfView: function(){
	       var element = document.getElementById("form");
	       html2pdf(element);
	   }	
	});
});