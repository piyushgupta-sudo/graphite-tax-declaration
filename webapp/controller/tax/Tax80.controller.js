sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"jquery.sap.global",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/m/UploadCollectionParameter",
	"com/infocus/fi/tax/ztaxapp/model/formatter"
], function(BaseController, jQuery, Fragment, MessageBox, MessageToast, Filter, UploadCollectionParameter, formatter) {
	"use strict";

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.Tax80", {
		_oValueOnFileUpload: null,
		formatter: formatter,

		onInit: function() {
			//var oRouter = this.getRouter();

			//	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
			this.getOwnerComponent().getEventBus().subscribe("Default", "get80Data", () => {
				this._onRead80DataSet();
			});
			this._initialDisplay();
		},

		viewUploadedFile: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");
			var oSec80Data = this.getView().getModel("sec80").getData();
			oSec80Data.buttonValue = buttonValue;
			this.getView().getModel("sec80").setData(oSec80Data);
			/*var btn80 = this.getView().byId("btn80c");*/

			// fragment open 
			if (!this._oValueOnFileView) {
				this._oValueOnFileView = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.view.FileUpload80View", this);
				this._oValueOnFileView.setModel(that.getOwnerComponent().getModel("FileUpload80View"));
				this.getView().addDependent(this._oValueOnFileView);

			}
			this._oValueOnFileView.open();

			// get the list of file from server 
			var oModel = that.getOwnerComponent().getModel();
			var oItemModel = that.getOwnerComponent().getModel("itemsData");
			var oItemData = oItemModel.getData();

			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			var oDescFilter = new sap.ui.model.Filter("Zdesc", sap.ui.model.FilterOperator.EQ, oSec80Data.buttonValue);
			/*var oFiscalYearFilter = new sap.ui.model.Filter("fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);*/
			sap.ui.core.BusyIndicator.show();
			oModel.read("/zfileSet", {

				filters: [oPernerFilter, oDescFilter],
				//In the case of success, the existing documents are in oData.results
				success: function(response) {
					var oData = response.results;

					oItemModel.setData(oData);
					console.log(oItemModel);
					sap.ui.core.BusyIndicator.hide();

				},
				error: function(error) {
					console.log(error);
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		/* file upload logic*/
		openFileUpload: function(oEvent) {
			var that = this;
			var oButton = oEvent.getSource();
			var buttonValue = oButton.data("value");
			var fileIndex = oEvent.mParameters.id.split("_")[3];
			var oSec80Data = this.getView().getModel("sec80").getData();
			oSec80Data.buttonValue = buttonValue;
			this.getView().getModel("sec80").setData(oSec80Data);

			if (!this._oValueOnFileUpload) {
				this._oValueOnFileUpload = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.FileUpload80", this);
				this._oValueOnFileUpload.setModel(that.getOwnerComponent().getModel("FileUpload80"));
				this.getView().addDependent(this._oValueOnFileUpload);
			}
			this._oValueOnFileUpload.open();
			var btn80ViewFileString = "btn80ViewFile_" + fileIndex;
			var btn80ViewFile = that.getView().byId(btn80ViewFileString);
			btn80ViewFile.setEnabled(true);
		},
		onCancelPress: function(oEvent) {
			this._oValueOnFileUpload.close();

		},
		onCancelPressView: function(oEvent) {
			this._oValueOnFileView.close();
			this.byId('edit').setVisible(false);

		},

		onChangeFileUpload: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oDataModel = this.getView().getModel();
			oDataModel.refreshSecurityToken();
			var oHeaders = oDataModel.oHeaders;
			var sToken = oHeaders['x-csrf-token'];

			/*(sToken)? MessageToast.show("") : MessageToast.show("Token is not Available");*/

			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: sToken
			});
			// add token as parameter
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},

		/*		onFileDeleted: function(oEvent) {
					MessageToast.show("Event fileDeleted triggered");
				},*/

		/*		onFilenameLengthExceed: function(oEvent) {
					MessageToast.show("Event filenameLengthExceed triggered");
				},*/

		/*		onFileSizeExceed: function(oEvent) {
					MessageToast.show("Event fileSizeExceed triggered");
				},*/

		/*		onTypeMissmatch: function(oEvent) {
					MessageToast.show("Event typeMissmatch triggered");
				},*/

		onStartUpload: function(oEvent) {
			var that = this;
			var oDataModel = this.getView().getModel();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollection80");
			var cFiles = oUploadCollection.getItems().length;

			if (cFiles > 0) {
				var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
				oUploadCollection.setUploadUrl(sUrl);
				oUploadCollection.upload();
			}
		},

		onBeforeUploadStarts: function(oEvent) {
			var oSec80Data = this.getView().getModel("sec80").getData();
			var oUploadCollection = sap.ui.getCore().byId("UploadCollection80");
			var cFiles = oUploadCollection.getItems().length;
			var oComponentName = oSec80Data.buttonValue;

			var oValue = [];
			oValue.push(oComponentName);
			oValue.push(oEvent.getParameter("fileName"));

			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: oValue
			});

			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
			/*setTimeout(function() {
				MessageToast.show("Event beforeUploadStarts triggered");
			}, 2000);*/
		},

		onUploadComplete: function(oEvent) {
			var that = this;
			var oSec80Data = this.getView().getModel("sec80").getData();
			var oComponentName = oSec80Data.buttonValue;
			var sResponse = oEvent.mParameters.files[0].response;

			var finalResponse = [];
			var result = Number(sResponse.split("/")[8].split("'")[1]).toString();
			finalResponse.push(result);
			var sStatus = oEvent.mParameters.files[0].status;

			if (sStatus === 201) {
				that.onCancelPress();
				MessageBox.information(oComponentName + "," + " ' " + finalResponse + " ' " + " Document Generated Successfully.");
				oUploadCollection.removeAllAggregation("items");
			} else {
				that.onCancelPress();
				MessageBox.error(sResponse + " " + " File not Uploaded !");
			}
		},

		/*		onSelectChange: function(oEvent) {
					var oUploadCollection = this.byId("UploadCollection");
					oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
				},*/

		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);
		},
		handleEditPress: function() {
			this._toggleButtonsAndView(true);
		},
		handleCancelPress: function() {
			this._toggleButtonsAndView(false);
		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);
		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);
		},
		_onRead80DataSet: function() {
			var that = this;

			var oModel = this.getOwnerComponent().getModel();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			//var oPernerFilter = new Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			//var oPernerFilter = new Filter("Pernr", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new Filter("Fiscal", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("Fiscal", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/Tax80Set";

			oModel.read(oUrl, {
				filters: [oFiscalYearFilter],
				success: function(response) {
					var data = response.results;
					console.log(data);
					var oJsonSec80cModel = that.getOwnerComponent().getModel('sec80');
					oJsonSec80cModel.setData(data);

					var oJsonGlobalData = that.getOwnerComponent().getModel("globalData").getData();
					var total = 0;
					oJsonGlobalData.oTotal80DeclaredAmount = 0;

					var headerModel = that.getOwnerComponent().getModel('employeeData').getData();

					// proposed & actual logic based on flag indicater
					if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
							' ')) {

						var newData = data[0];
						var strj;
						for (var j = 1; j <= 40; j++) {
							strj = '';
							if (j < 10) {
								strj = '0' + j.toString();
							} else {
								strj = j.toString();
							}
							var str = "Pcn" + strj;
							total += Number(newData[str]);
						}
						oJsonGlobalData.oTotal80DeclaredAmount += total;
					} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[
								0]
							.actual === 'X')) {

						// for (var i = 0; i < data.length; i++) {
						var newData = data[0];
						var strj;
						for (var j = 1; j <= 40; j++) {
							strj = '';
							if (j < 10) {
								strj = '0' + j.toString();
							} else {
								strj = j.toString();
							}
							var str = "Acn" + strj;
							total += Number(newData[str]);
						}
						// }
						oJsonGlobalData.oTotal80DeclaredAmount += total;
					}

					that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);

				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				var oIdProposed;
				var oIdActual;
				var btn80Id;

				if ((headerModel[0] !== undefined && headerModel[0].projection === 'X') || (headerModel[0] !== undefined && headerModel[0].actual ===
						' ')) {
					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);

					for (var i = 1; i <= 40; i++) {
						oIdProposed = "editItemProposed" + i.toString();
						this.byId(oIdProposed).setEnabled(true);
					}

					for (var j = 1; j <= 40; j++) {
						oIdActual = "editItemActual" + j.toString();
						this.byId(oIdActual).setEnabled(false);
					}

					for (var k = 1; k <= 40; k++) {
						btn80Id = "btn80_" + k.toString();
						this.byId(btn80Id).setEnabled(false);
					}

				} else if ((headerModel[0] !== undefined && headerModel[0].projection === ' ') || (headerModel[0] !== undefined && headerModel[0]
						.actual === 'X')) {

					this.byId('edit').setEnabled(true);
					this.byId('edit').setVisible(true);

					for (var i = 1; i <= 40; i++) {
						oIdProposed = "editItemProposed" + i.toString();
						this.byId(oIdProposed).setEnabled(false);
					}

					for (var j = 1; j <= 40; j++) {
						oIdActual = "editItemActual" + j.toString();
						this.byId(oIdActual).setEnabled(true);
					}
					for (var k = 1; k <= 40; k++) {
						btn80Id = "btn80_" + k.toString();
						this.byId(btn80Id).setEnabled(false);
					}

				} else {
					this.byId('edit').setEnabled(false);
					this.byId('edit').setVisible(false);
					this.byId('save').setVisible(false);
					this.byId('cancel').setVisible(false);
					this.byId("editItemId").setVisible(false)
					this.byId("displayItemId").setVisible(true);

				}
			}.bind(this));
		},
		onChange: function(oEvent) {

			var dataValid = parseInt(oEvent.getSource().getValue());

			// negative value not accepted
			/*			if (dataValid < 0) {
							MessageToast.show("input field can't hold negative value: " + dataValid);
							return oEvent.getSource().setValue("");
						}*/

			var oData = this.getOwnerComponent().getModel('sec80').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			oJsonGlobalData.oTotal80DeclaredAmount = 0;

			for (var i = 0; i < oData.length; i++) {
				/*oJsonGlobalData.oTotal80DeclaredAmount += Number(oData[i].Amount);*/
				var newData = oData[i];
				var strj;
				for (var j = 1; j <= 40; j++) {
					strj = '';
					if (j < 10) {
						strj = '0' + j.toString();
					} else {
						strj = j.toString();
					}
					var str = "Pcn" + strj;
					oJsonGlobalData.oTotal80DeclaredAmount += Number(newData[str]);
				}

			}

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},
		onSave: function() {

			var dataArray = this.getOwnerComponent().getModel('sec80').getData();

			for (var i = 0; i < dataArray.length; i++) {
				delete dataArray[i].__metadata;
			}

			// var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			// if (oJsonGlobalData.dataValidInt < 0) {
			// 	MessageBox.information("Input Amount can't be negative");
			// } else if (oJsonGlobalData.dataValidInt === 0) {
			// 	MessageBox.information("Input Amount can't be zero");
			// } else {

			// 	/*this.onUploadFile(dataArray);*/
			// 	this._onSave80DataSet(dataArray);
			// }
			this._onSave80DataSet(dataArray);
		},
		_onSave80DataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/Tax80Set";
			oModel.create(oUrl, oData[0], {
					success: function(response) {
						MessageBox.information("The 80D data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		},
		handleUploadComplete: function(oEvent) {
			var sMsg = " ";
			var sMsgType = "Error";
			var sResponse = oEvent.getParameter("response");
			var sStatus = oEvent.getParameter("status");

			if (sStatus === 201) {
				MessageBox.information(sResponse + "File Uploaded");
				/*debugger;*/
			} else {
				MessageBox.error(sResponse + "File not Uploaded !");
			}

			console.log(sResponse);
			/*debugger;*/
		},
		onUploadFile: function(oData) {
			var that = this;
			/*	var str = " ";
				var oFile = [];

				for (var i = 1; i <= 33; i++) {

					str = "fileUploaderFS" + i.toString();

					var oFileUploader = that.getView().byId(str);
					var sFile = oFileUploader.getValue();
					oFile.push(sFile);

				}

				console.log(oFile);*/

			var oFileUploader = that.getView().byId("UploadCollection");
			var sFile = oFileUploader.getValue();

			if (!sFile) {
				var sMsg = "Please select a file first";
				sap.m.MessageToast.show(sMsg);
				return;
			} else if (sFile) {
				var that = this;
				that.addTokenToUploader();

			}

		},
		addTokenToUploader: function() {

			var oDataModel = this.getView().getModel();
			var sTokenForUpload = oDataModel.getSecurityToken();
			// get the file value 
			/*var oFileUploader = this.getView().byId(str);
			var sFile = oFile;*/
			var that = this;

			/*			for (var i = 1; i <= 33; i++) {

							str = "fileUploaderFS" + i.toString();

							var oFileUploader = that.getView().byId(str);
							var sFile = oFileUploader.getValue();

							// add header parameter
							var oHeaderParameter = new sap.ui.unified.FileUploaderParameter({
								name: "X-CSRF-Token",
								value: sTokenForUpload
							});
							var oHeaderSlug = new sap.ui.unified.FileUploaderParameter({
								name: "SLUG",
								value: sFile
							});

							// header parameter removed to added
							oFileUploader.removeAllHeaderParameters();
							oFileUploader.addHeaderParameter(oHeaderParameter);
							oFileUploader.addHeaderParameter(oHeaderSlug);
							// set upload URL
							var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
							oFileUploader.setSendXHR(true);
							oFileUploader.setUploadUrl(sUrl);
							oFileUploader.upload();

						}*/

			var oFileUploader = that.getView().byId("UploadCollection");
			var sFile = oFileUploader.getValue();

			// add header parameter
			var oHeaderParameter = new sap.ui.unified.FileUploaderParameter({
				name: "X-CSRF-Token",
				value: sTokenForUpload
			});
			var oHeaderSlug = new sap.ui.unified.FileUploaderParameter({
				name: "SLUG",
				value: sFile
			});

			// header parameter removed to added
			oFileUploader.removeAllHeaderParameters();
			oFileUploader.addHeaderParameter(oHeaderParameter);
			oFileUploader.addHeaderParameter(oHeaderSlug);
			// set upload URL
			var sUrl = oDataModel.sServiceUrl + "/Uplod_DocSet";
			oFileUploader.setSendXHR(true);
			oFileUploader.setUploadUrl(sUrl);
			oFileUploader.upload();

		},
		onLiveChangeActual: function(oEvent) {
			var that = this;
			var dataLiveValid = parseInt(oEvent.getSource().getValue());
			var oData = this.getOwnerComponent().getModel('sec80').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			// selecting the button ID
			
			var indexParam = Number(oEvent.mParameters.id.slice(-2).replace(/\D/g,""))-1;
			var btn80IdArray = [];
			var btn80Id;
			for (var l = 1; l <= 40; l++) {
				btn80Id = "btn80_" + l.toString();
				btn80IdArray.push(btn80Id);
			}
			var btn80IdActual = btn80IdArray[indexParam];

			// Button disable enable & oTotaldeclaredValue added 
			if (dataLiveValid !== NaN && dataLiveValid > 0) {

				that.byId(btn80IdActual).setEnabled(true);
				oJsonGlobalData.oTotal80DeclaredAmount = 0;

				// for (var i = 0; i < oData.length; i++) {
				var newData = oData[0];
				var strj;
				for (var j = 1; j <= 40; j++) {
					strj = '';
					if (j < 10) {
						strj = '0' + j.toString();
					} else {
						strj = j.toString();
					}
					var str = "Acn" + strj;
					oJsonGlobalData.oTotal80DeclaredAmount += Number(newData[str]);
				}
				// }
				/*alert(oJsonGlobalData.oTotal80CAmountDeclared);*/
				that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);

			} else {
				that.byId(btn80IdActual).setEnabled(false);
				oJsonGlobalData.oTotal80DeclaredAmount = 0;

				// for (var i = 0; i < oData.length; i++) {
				var newData = oData[0];
				var strj;
				for (var j = 1; j <= 40; j++) {
					strj = '';
					if (j < 10) {
						strj = '0' + j.toString();
					} else {
						strj = j.toString();
					}
					var str = "Acn" + strj;
					oJsonGlobalData.oTotal80DeclaredAmount += Number(newData[str]);
				}
				// }
				/*alert(oJsonGlobalData.oTotal80CAmountDeclared);*/
				that.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
			}

			/*(dataLiveValid !== undefined && dataLiveValid > 0) ? that.byId(btn80IdActual).setEnabled(true): (that.byId(btn80IdActual).setEnabled(false) && oEvent.getSource().setValue(""));*/

		},

		onChangeProposed: function(oEvent) {

			var dataValid = parseInt(oEvent.getSource().getValue());

			// negative value not accepted
			/*			if (dataValid < 0) {
							MessageToast.show("input field can't hold negative value: " + dataValid);
							return oEvent.getSource().setValue("");
						}*/

			var oData = this.getOwnerComponent().getModel('sec80').getData();
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();

			oJsonGlobalData.oTotal80DeclaredAmount = 0;

			for (var i = 0; i < oData.length; i++) {
				var newData = oData[i];
				var strj;
				for (var j = 1; j <= 40; j++) {
					strj = '';
					if (j < 10) {
						strj = '0' + j.toString();
					} else {
						strj = j.toString();
					}
					var str = "Pcn" + strj;
					oJsonGlobalData.oTotal80DeclaredAmount += Number(newData[str]);
				}
			}

			// dataValidInt variable
			oJsonGlobalData.dataValidInt = dataValid;
			this.getOwnerComponent().getModel("globalData").setData(oJsonGlobalData);
		},

	});

});