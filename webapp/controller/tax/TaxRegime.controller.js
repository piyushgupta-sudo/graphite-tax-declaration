sap.ui.define([
	"com/infocus/fi/tax/ztaxapp/controller/BaseController",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/Filter"
], function(BaseController, Fragment, MessageBox, MessageToast, Filter) {
	"use strict";

	return BaseController.extend("com.infocus.fi.tax.ztaxapp.controller.tax.TaxRegime", {
		_oValueHelpTaxRegimeDialog: null,

		onInit: function() {
			//var oRouter = this.getRouter();

			//	oRouter.getRoute("employee").attachMatched(this._onRouteMatched, this);
			//this._onReadHRADataSet();
			this._initialDisplay();
			this.getOwnerComponent().getEventBus().subscribe("Default", "getTaxRegimeData", () => {
				/*this._getTaxRegimeType();*/
				this.__onReadTaxRegimeDataSet();
			});

		},
		handleTaxRegimeValueHelp: function() {
			var that = this;
			if (!this._oValueHelpTaxRegimeDialog) {
				this._oValueHelpTaxRegimeDialog = sap.ui.xmlfragment("com.infocus.fi.tax.ztaxapp.view.fragment.RegimeType", this);
				this._oValueHelpTaxRegimeDialog.setModel(that.getOwnerComponent().getModel("RegimeType"));
				this.getView().addDependent(this._oValueHelpTaxRegimeDialog);
			}
			this._oValueHelpTaxRegimeDialog.open();
		},

		handleTaxRegimeClose: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			var oInput = this.byId("TaxRegimeInput");

			if (oSelectedItem) {
				var oValue1 = oSelectedItem.getTitle();
				var oValue2 = oSelectedItem.getDescription();
				var TaxRegimeData = this.getOwnerComponent().getModel("TaxRegime").getData();
				TaxRegimeData[0].Txopt = oValue1;
				TaxRegimeData[0].Text = oValue2;
				this.getOwnerComponent().getModel("TaxRegime").setData(TaxRegimeData);
			}

			/*var oSelectedItem = oEvent.getParameter("selectedItem"),
				oInput = this.byId("accInput");

			if (oSelectedItem) {
				var oValue = oSelectedItem.getTitle() + " " + oSelectedItem.getDescription();
				//this.byId("accInput").setValue(oValue);
				var oHouseTypeData = this.getOwnerComponent().getModel('HRA').getData();
				oHouseTypeData[0].housingType = oValue;
				this.getOwnerComponent().getModel('HRA').setData(oHouseTypeData);
			}

			if (!oSelectedItem) {
				oInput.resetProperty("");
			}*/
		},
		onDialogAccClose: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				var year = aContexts.map(function(oContext) {
					return oContext.getObject().Title;
				}).join(", ");
				MessageToast.show("You have chosen " + year);

			} else {
				MessageToast.show("No new item was selected.");
			}
			oEvent.getSource().getBinding("items").filter([]);
		},
		_onRouteMatched: function(oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			oView = this.getView();

			oView.bindElement({
				path: "/Employees(" + oArgs.employeeId + ")",
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function(oEvent) {
						oView.setBusy(true);
					},
					dataReceived: function(oEvent) {
						oView.setBusy(false);
					}
				}
			});
		},
		_initialDisplay: function() {
			this.byId("editItemId").setVisible(false);
			this.byId("displayItemId").setVisible(true);
		},
		handleEditPress: function() {
			this._toggleButtonsAndView(true);
		},
		handleCancelPress: function() {
			this._toggleButtonsAndView(false);
		},
		handleSavePress: function() {
			this.onSave();
			this._toggleButtonsAndView(false);
		},
		_toggleButtonsAndView: function(bEdit) {
			var oView = this.getView();
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);
			this.byId("editItemId").setVisible(bEdit);
			this.byId("displayItemId").setVisible(!bEdit);
		},
		/*		_getTaxRegimeType: function() {
					var that = this;
					var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
					var oModel = this.getOwnerComponent().getModel();
					var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ,oJsonGlobalData.userId);
					var oFiscalYearFilter = new sap.ui.model.Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);

					var oUrl = "/TaxRegimeSet";

					oModel.read(oUrl, {
						filters: [oPernerFilter,oFiscalYearFilter],
						success: function(response) {
							var data = response.results;
							var oJsonTaxRegimeModel = that.getOwnerComponent().getModel('RegimeType');
							oJsonTaxRegimeModel.setData(data);
						},
						error: function(error) {
							//console.log(error);
							MessageToast.show("Error in loading the Tax Regime Type" + error);
						}
					});

					oModel.attachRequestCompleted(function() {
						var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
						if (headerModel[0] !== undefined && headerModel[0].projection === 'X') {
							this.byId('edit').setEnabled(true);
							this.byId('edit').setVisible(true);
						} else {
							this.byId('edit').setEnabled(false);
							this.byId('edit').setVisible(false);
							this.byId('save').setVisible(false);
							this.byId('cancel').setVisible(false);
						}

						this.byId("editItemId").setVisible(false);
						this.byId("displayItemId").setVisible(true);
					}.bind(this));
				}*/
		__onReadTaxRegimeDataSet: function() {
			var that = this;
			var oJsonGlobalData = this.getOwnerComponent().getModel("globalData").getData();
			var oModel = this.getOwnerComponent().getModel();
			var oPernerFilter = new Filter("Pernr", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.userId);
			/*console.log(oPernerFilter);*/
			//var oPernerFilter = new sap.ui.model.Filter("personnelNo", sap.ui.model.FilterOperator.EQ, "1000");
			var oFiscalYearFilter = new sap.ui.model.Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, oJsonGlobalData.selectedYear);
			//var oFiscalYearFilter = new Filter("fiscalYear", sap.ui.model.FilterOperator.EQ, "2022-2023");
			var oUrl = "/TaxRegimeSet";

			oModel.read(oUrl, {
				filters: [oPernerFilter, oFiscalYearFilter],
				success: function(response) {
					var data = response.results;

					var oJsonTaxRegimeModel = that.getOwnerComponent().getModel('TaxRegime');
					oJsonTaxRegimeModel.setData(data);
				},
				error: function(error) {
					//console.log(error);
					MessageToast.show("Error in loading the Financial Years" + error);
				}
			});
			oModel.attachRequestCompleted(function() {
				var headerModel = this.getOwnerComponent().getModel('employeeData').getData();
				if (headerModel[0] !== undefined && headerModel[0].projection === 'X') {
					this.byId('edit').setEnabled(false);
					this.byId('edit').setVisible(false);
				} else {
					this.byId('edit').setEnabled(false);
					this.byId('edit').setVisible(false);
					/*this.byId('edit').setVisible(false);
					this.byId('save').setVisible(false);
					this.byId('cancel').setVisible(false);*/
				}
			}.bind(this));
		},

			onSave: function() {
			var dataArray = this.getOwnerComponent().getModel("TaxRegime").getData();

			this._onSaveTaxRegimeDataSet(dataArray);
		},

		_onSaveTaxRegimeDataSet: function(oData) {
			var oModel = this.getOwnerComponent().getModel();
			var oUrl = "/TaxRegimeSet";
			//var oPernerFilter = new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, '1000');
			//var oFiscalYearFilter = new sap.ui.model.Filter("Fiscal", sap.ui.model.FilterOperator.EQ, '2022-2023');
			oModel.create(oUrl, oData[0], {
					success: function(response) {
						MessageBox.information("Tax Regime data is saved");
					},
					error: function(error) {
						var errorObject = JSON.parse(error.responseText);
						MessageBox.error(errorObject.error.message.value);
					}
				}

			);
		}

	});

});