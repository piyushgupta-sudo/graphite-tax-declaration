sap.ui.define([], function() {
	"use strict";
	return {
		currencyValue: function(amount) {
			if (!amount) {
				return "";
			}
			return parseFloat(amount).toFixed(2);
		},
		foramtYear: function(year) {
			var t = "";
			if (year) {
				var n = parseInt(year, 10);
				return (n - 1).toString() + "-" + n.toString().substring(2, 4);
			}
			return t;
		}
	};
});