sap.ui.define([
	] , function () {
		"use strict";

		return {

			/**
			 * Rounds the number unit value to 2 digits
			 * @public
			 * @param {string} sValue the number string to be rounded
			 * @returns {string} sValue with 2 digits rounded
			 */
			numberUnit : function (sValue) {
				if (!sValue) {
					return "";
				}
				return parseFloat(sValue).toFixed(2);
			},
			formatURL: function(Pernr, oDesc, Filename) {
		    
			console.log(Pernr,oDesc,Filename);

			var sRootUrl = this.getView().getModel().sServiceUrl;

			console.log("formatter:sRootUrl", sRootUrl);

			var sPath = sRootUrl + this.getView().getModel().createKey("/zfileSet", {
				Pernr: Pernr,
				Zdesc: oDesc,
				Filename: Filename
			}) + "/$value";

			console.log("formatter:sPath", sPath);
			return sPath;
			
		}

		};

	}
);